using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileData : MonoBehaviour
{
    
    private float speed;

    // Start is called before the first frame update
    void Start()
    {
        speed = 5 * Time.deltaTime;
    }

    // Update is called once per frame
    void Update()
    {

        transform.localRotation = Quaternion.Euler(0, 0, 0);
        transform.position = new Vector3(transform.position.x, transform.position.y - speed, transform.position.z);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("I see");

        if (collision.CompareTag("DeathZone") || collision.CompareTag("Player"))
        {
            Destroy(this.gameObject);
        }
    }

/*
    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log("I see");

        if (collision.collider.CompareTag("DeathZone") || collision.collider.CompareTag("Player"))
        {
            Destroy(this.gameObject);
        }
    }

*/}
