using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFragateData : MonoBehaviour
{
    [SerializeField] private float speed;

    [SerializeField] public List<GameObject> Objects = new List<GameObject>();

    private float xMin;
    private float yMin;
    private float waitingForNextSpawn = 3;
    private float theCountdown;
    // Start is called before the first frame update
    void Start()
    {

        theCountdown = waitingForNextSpawn;
        speed = 0.05f * Time.deltaTime;
        // speed = 0f;
    }

    // Update is called once per frame
    void Update()
    {
        xMin = transform.position.x;
        yMin = transform.position.y;
        transform.position = new Vector3(transform.position.x, transform.position.y - speed, transform.position.z);
        theCountdown -= Time.deltaTime;
        if (theCountdown <= 0)
        {
            spawnStuff();
            theCountdown = waitingForNextSpawn;
        }

    }
    private void spawnStuff()
    {
        Vector2 pos = new Vector2(xMin, yMin);
        GameObject objectToSpawn = Objects[0];
        Instantiate(objectToSpawn, pos, transform.rotation);

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("I see");

        if (collision.CompareTag("AllyBullet"))
        {
            Destroy(this.gameObject);
        }
        if (collision.CompareTag("DeathZone"))
        {
            Destroy(this.gameObject);
        }
    }
  
    /*
    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log("I see");

        if (collision.collider.CompareTag("AllyBullet"))
        {
            Destroy(this.gameObject);
        }
        if (collision.collider.CompareTag("DeathZone"))
        {
            Destroy(this.gameObject);
        }
    }*/
}
