using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerData : MonoBehaviour
{
    [SerializeField] public List<GameObject> Objects = new List<GameObject>();

    private float waitingForNextSpawn = 3;
    private float theCountdown;
    private float waitingForNextSpawnSpawner = 4;
    private float theCountdownspawner;

    public float xMin;
    public float xMax;


    public float yMin;
    // Start is called before the first frame update
    void Start()
    {
        theCountdownspawner = waitingForNextSpawnSpawner;
        theCountdown = waitingForNextSpawn;
        xMin = -7.2f;
        xMax = 7.2f;
        yMin = 5.1f;
    }

    // Update is called once per frame
    void Update()
    {
        theCountdown -= Time.deltaTime;
        if (theCountdown <= 0)
        {
            spawnStuff();
            theCountdown = waitingForNextSpawn;
        }
    }

    private void spawnStuff()
    {
        if (theCountdownspawner >= 1)
        {
            theCountdownspawner--;
            Vector2 pos = new Vector2(Random.Range(xMin, xMax), yMin);
            GameObject objectToSpawn = Objects[0];
            Instantiate(objectToSpawn, pos, transform.rotation);
        }
        else
        {
            
            Vector2 pos = new Vector2(Random.Range(xMin, xMax), yMin);
            GameObject objectToSpawn = Objects[1];
            Instantiate(objectToSpawn, pos, transform.rotation);
            theCountdownspawner = waitingForNextSpawnSpawner;
        }
       

    }
}
