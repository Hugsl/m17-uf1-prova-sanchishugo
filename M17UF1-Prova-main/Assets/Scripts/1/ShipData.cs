using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ShipData : MonoBehaviour
{

    [SerializeField] public List<GameObject> Objects = new List<GameObject>();
    private float speed;
    
    private float xMin;
    private float yMin;
   
    
    // Start is called before the first frame update
    void Start()
    {
        speed = 0.5f * Time.deltaTime;

    }

    // Update is called once per frame
    void Update()
    {
        xMin = transform.position.x;
        yMin = transform.position.y;
        
        moving();
        shoot();
    }

    private void moving()
    {
         transform.position = new Vector3((Input.GetAxis("Horizontal") * speed) + transform.position.x, (Input.GetAxis("Vertical") * speed) + transform.position.y, transform.position.z);
     
    }
    private void shoot()
    {
        if (Input.GetKeyDown("space"))
        {
            spawnStuff();
        }
    }


    private void spawnStuff()
    {
        Vector2 pos = new Vector2(xMin, yMin);
        GameObject objectToSpawn = Objects[0];
        Instantiate(objectToSpawn, pos, transform.rotation);

    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("I see");

        if (collision.CompareTag("Enemy"))
        {
            SceneManager.LoadScene("StartScene");
        }
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log("I see");
        
        if (collision.collider.CompareTag("Enemy"))
        {
            SceneManager.LoadScene("StartScene");

        }


    }
}
