using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletData : MonoBehaviour
{
    private float speed;
    // Start is called before the first frame update
    void Start()
    {
        speed = 10*Time.deltaTime;
    }

    // Update is called once per frame
    void Update()
    {
        transform.localRotation = Quaternion.Euler(0, 180, 0);
        transform.position = new Vector3(transform.position.x, speed + transform.position.y, transform.position.z);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("I see");

        if ( collision.CompareTag("Enemy"))
        {
            Destroy(this.gameObject);
        }
        else if (collision.CompareTag("DeathZone"))
        {
            Destroy(this.gameObject);
        }
    }
     private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log("I see");

        if (collision.collider.CompareTag("DeathZone") )
        {
            Destroy(this.gameObject);
        }
    }
}
